using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ArtGalleryProxy
{
    public class ArtStore
    {
        private readonly ILogger _logger;
        public ArtStore(string serverMountDirectory, ILogger logger)
        {
            ServerMountDirectory = serverMountDirectory;
            _logger = logger;

            Task.Run(ReadArtsFromDirectory);
        }

        /// <summary>
        /// Why async Task instead of void? Who knows - this looks better xD
        /// </summary>
        /// <returns></returns>
        private async Task ReadArtsFromDirectory()
        {
            while(Arts.Count == 0)
            {
                string[] paths = Directory.GetFiles(ServerMountDirectory).Where(p =>
                {
                    string ext = Path.GetExtension(p);
                    if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                        return true;
                    else
                        return false;
                }).ToArray();

                foreach(var path in paths)
                {
                    Arts.Add(new Art(path));
                }
                //File.Open(Path.Combine(ServerMountDirectory, ))

                await Task.Delay(5000);
            }
        }

        public List<Art> Arts { get; set; } = new List<Art>();
        public string ServerMountDirectory { get; set; } = "/mnt/nfs/Art";
        public ArtServerStatus ServerStatus 
        { 
            get
            {
                string test = $"-c \"mountpoint " + ServerMountDirectory + "\"";

                var process = new Process()
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "/bin/bash",
                        Arguments = $"-c \"mountpoint " + ServerMountDirectory + "\"",
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        UseShellExecute = false,
                        
                        CreateNoWindow = true
                    }
                };

                process.Start();
                //string output = process.StandardError.ReadToEnd();
                string output = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();

                process.WaitForExit();

                _logger.LogInformation(1, "ServerMountDirectory: " + ServerMountDirectory);
                _logger.LogInformation(1, "Server status output: " + output);
                _logger.LogInformation(1, "Server status error: " + error);

                #if DEBUG

                return ArtServerStatus.Available;

                #else

                if(output.EndsWith("is a mountpoint\n"))
                    return ArtServerStatus.Available;
                else
                    return ArtServerStatus.Unavailable;

                #endif
            } 
        }
    }
}