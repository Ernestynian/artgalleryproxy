using System;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace ArtGalleryProxy
{
    public class Thumbnail
    {
        /// <summary>
        /// In pixels
        /// </summary>
        public static int Height { get; set; } = 200;
        //public FileStream ParentImageFileStream { get; set; }
        public Size ParentSize { get; set; }
        public string ParentImageFilePath { get; set; }

        public Thumbnail(string parentImageFilePath)
        {
            ParentImageFilePath = parentImageFilePath;

            CreateThumbnailImage(parentImageFilePath);
        }

        private void CreateThumbnailImage(string parentImageFilePath)
        {
            if(File.Exists(ImageNameToThumbnailName(parentImageFilePath)))
                return;

            using(var image = new Bitmap(parentImageFilePath))
            {
                int width = ScaledWidth(image.Width, image.Height);

                Rectangle destRect = new Rectangle(0, 0, width, Height);
                Bitmap resultBitmap = new Bitmap(width, Height);
                //resultBitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                using(var graphics = Graphics.FromImage(resultBitmap))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        graphics.DrawImage(image, destRect, 0, 0, image.Width,image.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                }

                resultBitmap.Save(ImageNameToThumbnailName(ParentImageFilePath));
            }
        }

        public FileStream FileStream
        {
            get
            {
                return ReadThumbnailFromDisk();
            }
        }

        private int ScaledWidth(int width, int height)
        {
            float ratio = (float)height / (float)Height;
            return (int)((float)width / ratio);
        }

        private FileStream ReadThumbnailFromDisk()
        {
            try
            {
                return File.Open(ImageNameToThumbnailName(ParentImageFilePath), FileMode.Open);
            }
            catch(IOException)
            {
                return null;
            }
        }

        private string ImageNameToThumbnailName(string imageName)
        {
            return imageName + ".thumbnail";
        }
    }
}