# What
Simple, incomplete and ugly gallery for files from my PC when I feel like I need them.

# Why
My only public-facing server has about 30GB total disk space, so among other tasks it works as a proxy for a directory on my PC (via WireGuard, though it's not important for this piece of code to work).

## Why not an existing solution then
I had too much time at my hands, and probably nothing out there is as simple and lightweight as this.

## And why is it public
1) It could serve as a starting point for someone with similar mindset, provided they are really proficient with searching through repos on the internet.
2) It could help me with the job hunt (or do the opposite, but welp).

## And why is it unfinished
I didn't have clearly defined scope, so I lost my drive and left it as it was.

# Preview
![password prompt](RepoPresentationAssets/2.png "password prompt")
![after login](RepoPresentationAssets/1.png "after login")