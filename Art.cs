using System.IO;

namespace ArtGalleryProxy
{
    public class Art
    {
        public string Path { get; set; }
        public string Name 
        {
            get
            {
                return System.IO.Path.GetFileName(Path);
            }
        }
        public ArtType Type { get; set; }
        public Thumbnail Thumbnail { get; set; }
        public FileStream FileStream
        { 
            get
            {
                return File.Open(Path, FileMode.Open);
            }
        }

        public Art(string path)
        {
            Path = path;
            //FileStream = File.Open(Path, FileMode.Open);
            Thumbnail = new Thumbnail(Path);
        }
    }
}