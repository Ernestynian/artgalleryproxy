using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ArtGalleryProxy.Models;
using System.Threading.Tasks;

namespace ArtGalleryProxy
{
    public class AccountController : Controller
    {
        UserManager _userManager;
        public AccountController(UserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet("[controller]/login")]
        public IActionResult LogInView()
        {
            return View("Login"); 
        }

        [HttpPost]
        public async Task<IActionResult> LogIn(LogInViewModel form)
        {
            if (!ModelState.IsValid)
                return View(form);
            try
            {
                //authenticate
                var user = new UserModel()
                {
                    Password = form.Password
                };

                bool result = await _userManager.SignIn(this.HttpContext, user);
                if(result)
                    return RedirectToAction("Index", "Home", null);
                else
                    return View(form);//
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("summary", ex.Message);
                return View(form);
            }
        }
    }
}