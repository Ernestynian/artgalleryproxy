using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;

namespace ArtGalleryProxy.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ArtStore _artStore;
        private readonly ILogger<HomeController> _logger;
        public HomeController(ArtStore artStore, ILogger<HomeController> logger)
        {
            _artStore = artStore;
            _logger = logger;
        }

        public IActionResult Index()
        {
            if(_artStore.ServerStatus != ArtServerStatus.Available)
            {
                return View("ImageServerUnavailable");
            }

            return View();
        }

        public ActionResult<List<string>> GetArtsNames()
        {
            return _artStore.Arts.Select(a => a.Name).ToList();
        }

        [HttpGet("image/{name}")]
        public FileStreamResult GetFileByName(string name)
        {
            FileStream fs = null;
            string mimeType = null;

            Art art = _artStore.Arts.FirstOrDefault(a => a.Name == name);
            if(art != null)
            {
                fs = art.FileStream;
                mimeType = GetMIMEType(art.Name);
            }
            
            if(fs != null)
                return File(fs, mimeType);
            else
                return (FileStreamResult)(IActionResult)NotFound();
        }

        [HttpGet("image/{name}/thumbnail")]
        public IActionResult GetThumbnailByArtName(string name)
        {
            FileStream fs = _artStore.Arts.FirstOrDefault(a => a.Name == name)?.Thumbnail.FileStream;

            if(fs != null)
                return File(fs, "image/png");
            else
            return new NotFoundResult();
                //return (FileStreamResult)(IActionResult)NotFound();
        }

        private string GetMIMEType(string fileName)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if(!provider.TryGetContentType(fileName, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }
    }
}