using System.Collections.Generic;

namespace ArtGalleryProxy
{
    public enum ArtServerStatus
    {
        Available,
        Unavailable
    }
}