using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace ArtGalleryProxy
{
    public class UserManager
    {
        string hashFromConfig;
        /// <summary>
        /// 16
        /// </summary>
        byte[] saltFromConfig;
        ILogger<UserManager> _logger;
        public UserManager(ILogger<UserManager> logger, string hashFromConfig, string saltFromConfigString)
        {
            _logger = logger;
            
            if(hashFromConfig == null || saltFromConfigString == null)
            {
                _logger.LogError("Super user hash/salt missing from configuration.");
                return;//
            }

            this.hashFromConfig = hashFromConfig;
            this.saltFromConfig = saltFromConfigString.ToCharArray().Select(c => (byte)c).ToArray();
        }
        public async Task<bool> SignIn(HttpContext httpContext, UserModel user, bool isPersistent = false)
        {
            //super brillant password verification
            if(!VerifyPassword(user))
                return false;

            //typically here I would find the user in database and pass it to identity
            ClaimsIdentity identity = new ClaimsIdentity(GetUserClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
            return true;
        }

        private bool VerifyPassword(UserModel user)
        {
            string password = user.Password;
    
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = saltFromConfig;
    
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
                
            if(hashed == hashFromConfig)
                return true;
            else
                return false;
        }

        private IEnumerable<Claim> GetUserClaims(UserModel user)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, "1"));
            //claims.Add(new Claim(ClaimTypes.Name, user.UserFirstName));
            //claims.Add(new Claim(ClaimTypes.Email, user.UserEmail));
            //claims.AddRange(this.GetUserRoleClaims(user));
            return claims;
        }

        private IEnumerable<Claim> GetUserRoleClaims(UserModel user)
        {
            List<Claim> claims = new List<Claim>();

            //claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id().ToString()));
            //claims.Add(new Claim(ClaimTypes.Role, user.UserPermissionType.ToString()));
            return claims;
        }
    }
}