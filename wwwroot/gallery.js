var container = document.getElementById('images-container');

function onImageClick(element) {
    showFullImage(element.id);
}

function showFullImage(name) {
    //let container = document.getElementById('images-container');
    let imageContainer = document.createElement("div");
    imageContainer.className = "full-image-container";
    imageContainer.innerHTML = "<img class=\"full-image\" src=\"/image/" + name + "\"/>";
    imageContainer.onclick = function(){closeFullImage(imageContainer)};
    container.appendChild(imageContainer);
}

function closeFullImage(element) {
    //let container = document.getElementById('images-container');
    container.removeChild(element);
    
}